const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/backend/js/app.js')
    .sass('resources/sass/app.scss', 'public/backend/app.css');
mix.styles([
    'public/backend/assets/fonts/fontawesome/css/fontawesome-all.min.css',
    'public/backend/assets/fonts/material/css/materialdesignicons.min.css',
    'public/backend/assets/plugins/animation/css/animate.min.css',
    'public/backend/assets/plugins/notification/css/notification.min.css',
    'public/backend/assets/plugins/toolbar/css/jquery.toolbar.css',
    'public/backend/assets/plugins/ekko-lightbox/css/ekko-lightbox.min.css',
    'public/backend/assets/plugins/lightbox2-master/css/lightbox.min.css',
    'public/backend/assets/css/bootstrap-select.min.css',
    'public/backend/assets/plugins/modal-window-effects/css/md-modal.css',
    'public/backend/assets/plugins/select2/css/select2.min.css',
    'public/backend/assets/plugins/multi-select/css/multi-select.css',
    'public/backend/assets/plugins/data-tables/css/datatables.min.css',
    'public/backend/assets/css/custom.css'
], 'public/backend/css/backend-template.css');

mix.scripts([
    'public/backend/assets/js/vendor-all.min.js',
    'public/backend/assets/plugins/bootstrap/js/bootstrap.min.js',
    // 'public/backend/assets/js/menu-setting.min.js',
    'public/backend/assets/js/pcoded.js',
    'public/backend/assets/plugins/amchart/js/amcharts.js',
    'public/backend/assets/plugins/amchart/js/gauge.js',
    'public/backend/assets/plugins/amchart/js/serial.js',
    'public/backend/assets/plugins/amchart/js/light.js',
    'public/backend/assets/plugins/amchart/js/pie.min.js',
    'public/backend/assets/plugins/amchart/js/ammap.min.js',
    'public/backend/assets/plugins/amchart/js/usaLow.js',
    'public/backend/assets/plugins/amchart/js/radar.js',
    'public/backend/assets/plugins/amchart/js/worldLow.js',
    'public/backend/assets/plugins/notification/js/bootstrap-growl.min.js',
    // 'public/backend/assets/js/sweetalert2.all.min.js',
    'public/backend/assets/js/pages/dashboard-custom.js',
    'public/backend/assets/plugins/data-tables/js/datatables.min.js',
    'public/backend/assets/plugins/toolbar/js/jquery.toolbar.min.js',
    'public/backend/assets/js/pages/ac-toolbar.js',
    'public/backend/assets/plugins/ekko-lightbox/js/ekko-lightbox.min.js',
    'public/backend/assets/plugins/lightbox2-master/js/lightbox.min.js',
    'public/backend/assets/js/bootstrap-select.min.js',
    'public/backend/assets/plugins/modal-window-effects/js/classie.js',
    'public/backend/assets/plugins/modal-window-effects/js/modalEffects.js',
    'public/backend/assets/plugins/select2/js/select2.full.min.js',
    'public/backend/assets/plugins/multi-select/js/jquery.quicksearch.js',
    'public/backend/assets/plugins/multi-select/js/jquery.multi-select.js',
    'public/backend/assets/js/pages/form-select-custom.js',

], 'public/js/backend-template.js');
