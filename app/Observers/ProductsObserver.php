<?php

namespace App\Observers;

use App\Models\Product as AppProduct;
use App\Models\Product;
use App\ProductReport;

class ProductsObserver
{

    public function created(Product $product)
    {
        //
    }


    public function updated(Product $product)
    { }

    public function updating(Product $product)
    {
//        if ($product->isDirty('quantity')) {
//            $user = auth()->user() ? auth()->user() : auth('api')->user();
//            ProductReport::create([
//                'user' => $user->username ?? 'غير معروف',
//                'before' => $product->getOriginal('quantity'),
//                'after' => $product->quantity,
//                'product_id' => $product->id,
//                'user_id' => $user->id,
//                'user_type' => auth()->user() ? 'مشرف' : 'مستخدم'
//            ]);
//        }
    }

    function deleted(Product $product)
    {
        //
    }


    public function restored(Product $product)
    {
        //
    }


    public function forceDeleted(Product $product)
    {
        //
    }
}
