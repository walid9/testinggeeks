<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';
    protected $api_namespace = 'App\Http\Controllers\Api';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        // Api Routes Registering

        $this->mapApiAuthRoutes();

        $this->mapApiOtpRoutes();

        $this->mapApiProductsRoutes();

        $this->mapApiStoresRoutes();

        $this->mapApiCartRoutes();

        $this->mapApiProfileRoutes();

    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace)
            ->group(base_path('routes/api.php'));
    }

    // Api Routes


    protected function mapApiAuthRoutes()
    {
        Route::prefix('api/auth')
            ->namespace($this->api_namespace)
            ->middleware('api')
            ->group(base_path('routes/api/auth.php'));

    }

    protected function mapApiStoresRoutes()
    {
        Route::prefix('api/stores')
            ->namespace($this->api_namespace)
            ->middleware('api')
            ->group(base_path('routes/api/stores.php'));
    }

    protected function mapApiProductsRoutes()
    {
        Route::prefix('api/products')
            ->namespace($this->api_namespace)
            ->middleware('api')
            ->group(base_path('routes/api/products.php'));
    }

    protected function mapApiProfileRoutes()
    {
        Route::prefix('api/profile')
            ->namespace($this->api_namespace)
            ->middleware('api')
            ->group(base_path('routes/api/profile.php'));
    }

    protected function mapApiOtpRoutes()
    {
        Route::prefix('api/otp')
            ->namespace($this->api_namespace)
            ->middleware('api')
            ->group(base_path('routes/api/otp.php'));
    }

    protected function mapApiCartRoutes()
    {
        Route::prefix('api/cart')
            ->namespace($this->api_namespace)
            ->middleware(['api', 'jwt.auth'])
            ->group(base_path('routes/api/cart.php'));
    }

}
