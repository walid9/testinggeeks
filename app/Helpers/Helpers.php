<?php

use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
if (!function_exists('recentNotifications')) {
    function recentNotifications()
    {
        $notifications= DB::table('notifications')
            ->where('receiver_id', auth()->user()->id)
            ->where('is_read' , 0)
            ->join('notificationLocales', 'notifications.notification_key', '=', 'notificationLocales.notification_key')
            ->where('notificationLocales.language_id', 2)
            ->select('notifications.*', 'notificationLocales.heading', 'notificationLocales.content')
            ->orderBy('notifications.id' , 'desc')
            ->take(2)
            ->get();
        return $notifications;
    }
};
if (!function_exists('rate')) {
     function rate($id)
    {
        if(!ProductReview::where('product_id', $id)->first())
            return 0;

        return round((ProductReview::where('product_id', $id)->sum('rate')) / (count(ProductReview::where('product_id', $id)->get())));
    }
};
if (!function_exists('countNotifications')) {
    function countNotifications()
    {
        $notifications= DB::table('notifications')
            ->where('receiver_id', auth()->user()->id)
            ->where('is_read' , 0)
            ->join('notificationLocales', 'notifications.notification_key', '=', 'notificationLocales.notification_key')
            ->where('notificationLocales.language_id', 2)
            ->select('notifications.*', 'notificationLocales.heading', 'notificationLocales.content')
            ->orderBy('notificationLocales.id' , 'desc')
            ->get();
        return count($notifications);
    }
};


if (!function_exists('apiResponse')) {
    function apiResponse($status, $msg, $data = null, $force_data = false, $per_page = null)
    {
        if (!$data && !$force_data) {
            return [
                'status' => $status,
                'msg' => $msg,
            ];
        } else {
            return [
                'status' => $status,
                'msg' => $msg,
                'data' => $per_page ? manual_pagination($data, $per_page ?? $per_page) : $data
            ];
        }
    }
};

if (!function_exists('apiSupResponse')) {
    function apiSupResponse($status, $msg, $data = null, $force_data = false, $per_page = null)
    {
        if (!$data && !$force_data) {
            return [
                'status' => $status,
                'msg' => $msg,
            ];
        } else {
            //$data['whatsapp'] = Setting::where('name' , 'whatsapp')->first()->value;
            $pagination = manual_pagination($data, $per_page ?? $per_page);
            return [
                'status' => $status,
                'msg' => $msg,
                'data' => $per_page ? $pagination : $data,
                'whatsapp' => Setting::where('name' , 'whatsapp')->first()->value
            ];
        }
    }
};

if (!function_exists('adminNotification')) {
    function adminNotification($notification_key ,  $receiver_id , $notification_type , $channel_id , $data , $sending_response , $status_id)
    {
        if (!$data && !$receiver_id && !$channel_id && !$notification_key && !$notification_type) {
            $response = [
                'status' => 401,
                'msg' => 'Please Insert Full Fields Of Notification',
            ];
            return response()->json($response);
        }
        foreach($receiver_id as $value) {
            $notification = Notification::create([
                'notification_key' => $notification_key,
                'receiver_id' => $value,
                'notification_type' => $notification_type,
                'channel_id' => $channel_id,
                'data' => $data
            ]);
            Notification::where('id', $notification->id)->update(['sending_response' => $sending_response]);

            updateNotification($notification->id, $status_id);
        }

    }
};

if (!function_exists('userNotification')) {
    function userNotification($notification_key ,  $receiver_id , $notification_type , $channel_id , $data )
    {
        if (!$data && !$receiver_id && !$channel_id && !$notification_key && !$notification_type) {
            $response = [
                'status' => 401,
                'msg' => 'Please Insert Full Fields Of Notification',
            ];
            return response()->json($response);
        }
foreach($receiver_id as $value) {
    $notification = Notification::create([
        'notification_key' => $notification_key,
        'receiver_id' => $value,
        'notification_type' => $notification_type,
        'channel_id' => $channel_id,
        'data' => $data,
    ]);
    // $notification=Notification::orderBy('id' , 'desc')->first();
    $result = sendNotification($notification, $value);
    Notification::where('id', $notification->id)->update(['sending_response' => $result]);
    if ($result['response']['success'] == 1) {

        updateNotification($notification->id, 1);
    } else {
        updateNotification($notification->id, 2);
    }
   // updateNotification($notification->id, 1);
}

    }
};

if (!function_exists('offerNotification')) {
    function offerNotification($title , $body ,  $receiver_id , $notification_type , $channel_id , $data )
    {
        if (!$data && !$receiver_id && !$channel_id && !$title && !$body && !$notification_type) {
            $response = [
                'status' => 401,
                'msg' => 'Please Insert Full Fields Of Notification',
            ];
            return response()->json($response);
        }
        // $notification=Notification::orderBy('id' , 'desc')->first();
        foreach($receiver_id as $value) {
            sendOfferNotification($title , $body , $value , $data);
        }
        // Notification::where('id', $notification->id)->update(['sending_response' => $result]);
        /*if ($result['response']['success'] == 1) {

            updateNotification($notification->id, 1);
        } else {
            updateNotification($notification->id, 2);
        }*/
        // updateNotification($notification->id, 1);
    }

};

if (!function_exists('updateNotification')) {
    function updateNotification($notification_id , $status_id)
    {
        if (!$notification_id && !$status_id) {
            $response =  [
                'status' => 401,
                'msg' => 'Please Insert Full Fields To Update Notification',
            ];
            return response()->json($response);
        }
        else
        {
            Notification::where('id' , $notification_id)->update([
                'sending_id' => $status_id,
            ]);
            $response =  [
                'status' => 200,
                'msg' => trans('messages.success'),
            ];
            return response()->json($response);
        }
    }
};

if (!function_exists('manual_pagination')) {
    function manual_pagination($items, $perPage = 5, $page = null)
    {
        $pageName = 'page';
        $page = $page ?: (Paginator::resolveCurrentPage($pageName) ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator(
            $items->forPage($page, $perPage)->values(),
            ceil($items->count() / $perPage),
            $perPage,
            $page,
            [
                'path' => Paginator::resolveCurrentPath(),
                'pageName' => $pageName,
            ]
        );
    }
}

if (!function_exists('sidebarActive')) {
    function sidebarActive(array $routes)
    {
        if (in_array(Route::currentRouteName(), $routes)) {
            return 'active pcoded-trigger';
        }
        return '';
    }
}
if (!function_exists('set_active')) {
    function set_active($path, $active = 'active pcoded-trigger')
    {
        return call_user_func_array('Request::is', (array) $path) ? $active : '';
    }
}


if (!function_exists('generateRandomCode')) {
    function generateRandomCode($digits, $id = null)
    {
        $i = 0; //counter
        $value = $id; //our default pin is the order id .
        while ($i < $digits - intval(strlen($id))) {
            //generate a random number between 0 and 9.
            $value .= mt_rand(0, 9);
            $i++;
        }
         return $value;

    }
}

if (!function_exists('malePlaceHolder')) {
    function malePlaceHolder()
    {
        return str_random(60);
    }
}

if (!function_exists('generateRandomCode')) {
    function generateRandomCode($digits, $id = null)
    {
        $i = 0; //counter
        $value = $id; //our default pin is the order id .
        while ($i < $digits - intval(strlen($id))) {
            //generate a random number between 0 and 9.
            $value .= mt_rand(0, 9);
            $i++;
        }
         return $value;
        //return 1111; //TODO Disable In Production
    }


}


if (!function_exists('sendNotification')) {

    function sendNotification(Notification $notification ,$user_id)
    {

        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';

        $notificationHead='';

        if(User::where('id', $user_id)->first()->lang_code == 'ar') {
            $notificationHead = DB::table('notifications')
                ->where('notifications.id', $notification->id)
                ->where('notifications.is_read', 0)
                ->join('notificationLocales', 'notifications.notification_key', '=', 'notificationLocales.notification_key')
                ->where('notificationLocales.language_id', 2)
                ->select('notificationLocales.heading', 'notificationLocales.content', 'notifications.data')
                ->first();
        }
        elseif(User::where('id', $user_id)->first()->lang_code == 'en') {
            $notificationHead = DB::table('notifications')
                ->where('notifications.id', $notification->id)
                ->where('notifications.is_read', 0)
                ->join('notificationLocales', 'notifications.notification_key', '=', 'notificationLocales.notification_key')
                ->where('notificationLocales.language_id', 1)
                ->select('notificationLocales.heading', 'notificationLocales.content', 'notifications.data')
                ->first();
        }

            $notification = [
                'title' => optional($notificationHead)->heading,
                'body' => optional($notificationHead)->content,
                //'sound' => true,
            ];
            $data = [
                'title' => optional($notificationHead)->heading,
                'body' => optional($notificationHead)->content,
                'key' => optional($notificationHead)->data,
            ];
           // $tokenId = array();
        $tokenId[] = User::where('id', $user_id)->first()->firebase_token;
           // array_push($tokenId, $user->firebase_token);
            $fcmNotification = [
                'registration_ids' => $tokenId,
                'notification' => $notification,
                'data' => $data
            ];

            $headers = [
                'Authorization:key=AAAADD_iqiQ:APA91bF7oGFB1AxjMvBy0lItHfRUos1cOO9NNgVLIB7mcP6KNVChRaRXf5kNcFby7QjwE6XXMsNXVvhtxMjbwAGFGG9czPEBlf1b7lPD8Eqfv4ohuPzMpGxR8sV6FaQOZLSxWbiotHOM',
                'Content-Type: application/json'
            ];
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $fcmUrl);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
            $res_json = curl_exec($ch);
            $result = get_object_vars(json_decode($res_json));
            $code = array('info' => curl_getinfo($ch, CURLINFO_HTTP_CODE));
            $merge1 = array_merge($code, $fcmNotification);
            $merge = array_merge($result, $merge1);
            $reposeDet = array('response' => $merge);
            curl_close($ch);
            return $reposeDet;
        }

}


if (!function_exists('sendOfferNotification')) {

    function sendOfferNotification($title , $body ,$user_id , $data)
    {

        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';

        /* $notificationHead='';

         if(User::where('id', $user_id)->first()->lang_code == 'ar') {
             $notificationHead = DB::table('notifications')
                 ->where('notifications.id', $notification->id)
                 ->where('notifications.is_read', 0)
                 ->join('notificationLocales', 'notifications.notification_key', '=', 'notificationLocales.notification_key')
                 ->where('notificationLocales.language_id', 2)
                 ->select('notificationLocales.heading', 'notificationLocales.content', 'notifications.data')
                 ->first();
         }
         elseif(User::where('id', $user_id)->first()->lang_code == 'en') {
             $notificationHead = DB::table('notifications')
                 ->where('notifications.id', $notification->id)
                 ->where('notifications.is_read', 0)
                 ->join('notificationLocales', 'notifications.notification_key', '=', 'notificationLocales.notification_key')
                 ->where('notificationLocales.language_id', 1)
                 ->select('notificationLocales.heading', 'notificationLocales.content', 'notifications.data')
                 ->first();
         }*/
        /* $heading='';
         $content='';
         if(App::getLocale() == 'en') {
             $heading = $notificationHead->getTranslation('heading', 'en');
             $content = $notificationHead->getTranslation('content', 'en');
         }
         else {
             $heading = $notificationHead->getTranslation('heading', 'ar');
             $content = $notificationHead->getTranslation('content', 'ar');
         }*/
        $notification = [
            'title' => $title,
            'body' => $body,
            //'sound' => true,
        ];
        $data = [
            'title' => $title,
            'body' => $body,
             'key' => $data,
        ];
        // $tokenId = array();
        $tokenId[] = User::where('id', $user_id)->first()->firebase_token;
        // array_push($tokenId, $user->firebase_token);
        $fcmNotification = [
            'registration_ids' => $tokenId,
            'notification' => $notification,
            'data' => $data
        ];

        $headers = [
            'Authorization:key=AAAADD_iqiQ:APA91bF7oGFB1AxjMvBy0lItHfRUos1cOO9NNgVLIB7mcP6KNVChRaRXf5kNcFby7QjwE6XXMsNXVvhtxMjbwAGFGG9czPEBlf1b7lPD8Eqfv4ohuPzMpGxR8sV6FaQOZLSxWbiotHOM',
            'Content-Type: application/json'
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $res_json = curl_exec($ch);
        $result = get_object_vars(json_decode($res_json));
        $code = array('info' => curl_getinfo($ch, CURLINFO_HTTP_CODE));
        $merge1 = array_merge($code, $fcmNotification);
        $merge = array_merge($result, $merge1);
        $reposeDet = array('response' => $merge);
        curl_close($ch);
        return $reposeDet;
    }

}
