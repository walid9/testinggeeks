<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    public $table = 'oneTimePasswords';

    protected $dates = [
        'updated_at',
        'created_at',
    ];

    protected $fillable = [
        'name',
        'description',
        'user_id',

    ];

    public function user()
    {
        return $this->belongsTo(User::class, "user_id");
    }

    public function products()
    {
        return $this->hasMany(Product::class , 'store_id');
    }

}
