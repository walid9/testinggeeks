<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements HasMedia, JWTSubject
{
    use HasMediaTrait,  Notifiable, SoftDeletes, HasRoles;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';
    protected $appends = ['img'];
    protected $dates = [
        'updated_at',
        'created_at',
        'deleted_at',
        'email_verified_at',
    ];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "name",
        "email",
        "email_verified_at",
        "password",
        "type",
        "remember_token",
        "mobile_number",
        "device_id",
        "firebase_token",
        "active",
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }

    public function scopeFilterByInput($query, $value)
    {
        return $query->when($value, function ($row) use ($value) {
            $row->where('name', 'like', "%$value%")
                ->orWhere('email', 'like', "%$value%")
                ->orWhere('mobile_number', 'like', "%$value%");
        });
    }


    public function registerMediaCollections()
    {
        $this->addMediaCollection('user')
            ->singleFile();
    }
    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(50)->height(50);
    }
    public function getImgAttribute()
    {
        $file = $this->getMedia('user')->last();

        if ($file) {
            $file->url = $file->getUrl();
            $file->thumbnail = $file->getUrl('thumb');
            $file->localUrl = asset('storage/'.$file->id.'/'.$file->file_name);
        }

        return $file;
    }

    //    user cart
    public function userCart()
    {
        return $this->belongsToMany(Product::class, 'user_cart')->withTimestamps()
            ->withPivot(['id','quantity']);
    }


    public function getNameAttribute()
    {
        return $this->name;
    }
    public function setImageAttribute($value)
    {
        return $this->addMedia($value)->toMediaCollection('user');
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }


    public function getJWTCustomClaims()
    {
        return [];
    }

    public function stores()
    {
        return $this->hasMany(Store::class , 'user_id');
    }

}
