<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Spatie\Translatable\HasTranslations;
use Illuminate\Support\Facades\File;

class Product extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait;

    protected $with = ['productPrices'];
    protected $appends = ['api_images'];
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'name',
        'description',
        'selling_price',
        'purchasing_price',
        'quantity',
        'store_id',
        'updated_at',
        'deleted_at',
    ];

    public function registerMediaCollections()
    {
        $this->addMediaCollection('product');
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(50)->height(50);
    }

    public function getDefaultImageAttribute()
    {
        return  asset('storage/not_found.png');
    }

    public function getApiImagesAttribute()
    {
        $files = $this->getMedia('product');
        $urls = [];
        foreach ($files as  $file) {
            array_push($urls, $file->getFullUrl());
        }

        return $urls;
    }

    public function scopeFilterByInput($query, $value)
    {
        return $query->when($value, function ($row) use ($value) {
            $row->where('name', 'like', "%$value%")
                ->orWhere('description', 'like', "%$value%");
        });
    }

    public function store()
    {
        return $this->belongsTo(Store::class, "store_id");
    }

}
