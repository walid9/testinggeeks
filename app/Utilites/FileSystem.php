<?php

namespace App\Utilites;


use Illuminate\Support\Facades\File;
use Image;
use Illuminate\Support\Facades\Storage;

abstract class FileSystem
{
    const PUBLIC_FOLDER = 'public';

    public static function upload($file)
    {

        $filename = md5(date('Y-m-d-H:i:s')) . '_' . $file->getClientOriginalName();

        Storage::disk(Filesystem::PUBLIC_FOLDER)->put($filename , File::get($file));
        return $filename;
    }
}

