<?php


namespace App\Traits;
use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

trait OrderUpdateStatus
{
    public function firebaseCreate($order) {

        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
        $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->withDatabaseUri('https://baldi-d44a1.firebaseio.com/')
            ->create();

        $database   =   $firebase->getDatabase();
        $createPost    =   $database
            ->getReference('orders/'.$order['id'])
            ->push($order);

    }

    public function firebaseUpdate($order) {

        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
        $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->withDatabaseUri('https://baldi-d44a1.firebaseio.com/')
            ->create();

        $database = $firebase->getDatabase();

        $key = $database
            ->getReference('orders/'.$order['id'])->getChildKeys();

        $firebaseOrder = $database
            ->getReference('orders/'.$order['id'])->getChild($key[0]);

        $firebaseOrder->update(['order_status_id' => intval($order['order_status_id']) ]);
    }

}
