<?php

namespace App\Console\Commands;

use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CheckOrdersStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:order';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to check orders status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $orders = Order::where('created_at', '<', Carbon::now()->subMinutes(1))->whereHas('invoice', function ($q){

            $q->where('invoice_status_id', 2)->where('payment_type_id', 2); // 2=>pending , 2=>credit

        })->get();

        foreach($orders as $order){

            $order->order_status_id = 5;   // 5=>order canceled
            $order->save();

            $order->invoice->invoice_status_id = 4;  // 5=>invoice canceled

            $order->invoice->save();

            foreach($order->products as $product){

                $price = $product->productPrices()->where('id',$product->pivot->price_id)->first();
                $price->quantity += $product->pivot->quantity;

                $price->save();
            }
        }
    }
}
