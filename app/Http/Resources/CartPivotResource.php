<?php

namespace App\Http\Resources;

use App\Models\Beaf;
use App\Models\CuttingType;
use App\Models\HeadCutting;
use App\Models\PackagingType;
use App\Models\ProductExtraClassification;
use App\Models\ProductPrice;
use App\Models\ProductSize;
use Illuminate\Http\Resources\Json\JsonResource;

class CartPivotResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [

            'user_id'=> $this->user_id,
            'created_at'=> $this->created_at? $this->created_at->format('Y-m-d'):$this->created_at,
            'updated_at'=> $this->updated_at? $this->updated_at->format('Y-m-d') : $this->updated_at,
            'id'=> $this->id,
            'quantity'=> $this->quantity,
        ];
    }
}
