<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ListUsersResource extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->transform(function ($row) {
            return [
                'id' => $row->id,
                'name' => $row->name ?? "",
                'email' => $row->email ?? "",
                'mobile_number' => $row->mobile_number,
                'img' => $row->image ?  $row->image->url : "",
            ];
        })->toArray();    }
}
