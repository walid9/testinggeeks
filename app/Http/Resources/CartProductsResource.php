<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CartProductsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $price = $this->selling_price;

        $total = ($price * $this->pivot->quantity);

        $out_of_stock = false;

        if($price->quantity < $this->pivot->quantity)
            $out_of_stock = true;

        return [
            'id' => $this->id,
            'cart_id' => $this->pivot->id,
            'qty_in_stock' => $price->quantity,
            'out_of_stock' => $out_of_stock,
            'name' => $this->name,
            'description' => $this->description,
            'pivot' => new CartPivotResource($this->pivot),
            'product_price' => $price,

            'total'=> round($total,2),

            'images' => $this->api_images,

        ];
    }
}
