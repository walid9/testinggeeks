<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Carbon;

class StoresResource extends ResourceCollection
{

    public $user;

    public function __construct($resource,$user)
    {
        parent::__construct($resource);
        $this->user = $user;
    }

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->transform(function ($row) {

            return [
                'id' => $row->id,
                'name' => $row->name,
                'description' => $row->description,

            ];
        })->toArray();
    }
}
