<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Auth\ProfileImageRequest;
use App\Http\Requests\Api\Auth\ProfileRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;

class ProfileController extends Controller
{
    /**
     * @SWG\Get(
     *      path="/profile/",
     *      operationId="get User Information",
     *      tags={"profile"},
     *      summary="Get Profile information for user",
     *      description="Get Profile information for user",
     *
     *          @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),

     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:Profile", "read:Profile"}
     *         }
     *     },
     * )
     *
     */
    public function index(Request $request)
    {
       /* $user = \JWTAuth::parseToken()->authenticate();
        if(!$user)
        {
            return apiResponse(401, 'تسجيل الدخول مطلوب من اجل هذه العمليه');
        }*/
        if(request()->header('authorization')){

            try {
                $user = \JWTAuth::parseToken()->authenticate();

            } catch (JWTException $e) {

                return apiResponse(500, 'كود التسجيل خطا ، من فضلك ادخل الكود الصحيح');
            }
        }

        return apiResponse(200, trans('messages.success'), new UserResource($user));
    }

    /**
     * @SWG\Post(
     *      path="/profile/update",
     *      operationId="update profile for user",
     *      tags={"profile"},
     *      summary="update user profile",
     *      description="update user profile",
     *
     *          @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *          @SWG\Parameter(
     *          name="name",
     *          description=" name ",
     *          required=false,
     *          type="string",
     *          in="formData"
     *      ),
     *          @SWG\Parameter(
     *          name="email",
     *          description="user email",
     *          required=false,
     *          type="string",
     *          in="formData"
     *      ),
     *          @SWG\Parameter(
     *          name="mobile_number",
     *          description="user phone",
     *          required=false,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:category", "read:category"}
     *         }
     *     },
     * )
     *
     */

    public function update(ProfileRequest $request)
    {
       /*$user = \JWTAuth::parseToken()->authenticate();
        if(!$user)
        {
            return apiResponse(401, 'تسجيل الدخول مطلوب من اجل هذه العمليه');
        }*/
        if(request()->header('authorization')){

            try {
                $user = \JWTAuth::parseToken()->authenticate();

            } catch (JWTException $e) {

                return apiResponse(500, 'كود التسجيل خطا ، من فضلك ادخل الكود الصحيح');
            }
        }
      if($request->mobile_number) {
          if (User::where('id', '!=', $user->id)->where('mobile_number', $request->mobile_number)->first())
              return apiResponse(401, 'رقم الجوال مستخدم من قبل');
      }

      if($request->email) {
          if (User::where('id', '!=', $user->id)->where('email', $request->email)->first())
              return apiResponse(401, 'البريد الالكترونى مستخدم من قبل');
      }

        $user->update($request->all());
        return apiResponse(200, trans('messages.success'), new UserResource($user));
    }


    /**
     * @SWG\Post(
     *      path="/profile/uploadImage",
     *      operationId="update profile image for user",
     *      tags={"profile"},
     *      summary="update user image",
     *      description="update user image",
     *
     *      @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *          @SWG\Parameter(
     *          name="image",
     *          description="user image ",
     *          required=true,
     *          type="file",
     *          in="formData"
     *      ),

     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:category", "read:category"}
     *         }
     *     },
     * )
     *
     */

    public function uploadImage(ProfileImageRequest $request)
    {
       /* $user = \JWTAuth::parseToken()->authenticate();
        if(!$user)
        {
            return apiResponse(401, 'تسجيل الدخول مطلوب من اجل هذه العمليه');
        }*/
        if(request()->header('authorization')){

            try {
                $user = \JWTAuth::parseToken()->authenticate();

            } catch (JWTException $e) {

                return apiResponse(500, 'كود التسجيل خطا ، من فضلك ادخل الكود الصحيح');
            }
        }

            $file = $request->file('image');
            $user->addMedia($file)->toMediaCollection('user');

        return apiResponse(200, trans('messages.success'), new UserResource($user));
    }

}
