<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Auth\VerifyOtpRequest;
use App\Http\Resources\OtpResource;
use App\Models\Otp;
use App\Models\User;
use App\Traits\SendMessage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Mail;
use Tymon\JWTAuth\Exceptions\JWTException;

class OtpController extends Controller
{
    use SendMessage;
    /**
     * @SWG\Post(
     *      path="/otp/sendOtp",
     *      operationId="Send New Code to verify Phone",
     *      tags={"otps"},
     *      summary="Send New Code to user for verify Phone",
     *      description="Send New Code to user for verify Phone number",
     *          @SWG\Parameter(
     *          name="phone",
     *          description="phone of user",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:otps", "read:otps"}
     *         }
     *     },
     * )
     *
     */

    public function sendOtp(Request $request)
    {

        $user = User::where('mobile_number' , $request->phone)->first();

        if($user) {
//'code' => generateRandomCode(4)
            $otp = Otp::create([
                'user_id' => $user->id,
                'code' => generateRandomCode(4)
            ]);

            $email = $user->email;
            $code=$otp->code;
            $admin_email="bwardy.bander@gmail.com";

            try {

                $this->sendMessage($user->mobile_number,'The verification code is '.$otp->code );

                @Mail::send([], [], function ($message) use ($email, $admin_email, $code) {
                    $message->to($email)
                        ->subject('Mhsol')
                        ->setBody("Verification Code Is : " . $code);
                    $message->from($admin_email);
                });
            }
            catch (\Exception $e) {

                return apiResponse(200, trans('messages.success'), new OtpResource($otp));
            }

            return apiResponse(200, trans('messages.success'), new OtpResource($otp));
        }
        else
            return apiResponse(401, 'رقم الجوال الذي ادخلته غير صحيح');

    }

    /**
     * @SWG\Post(
     *      path="/otp/verifyOtp",
     *      operationId="verify Phone of new user",
     *      tags={"otps"},
     *      summary="verify Phone of new user by confirmation code",
     *      description="verify Phone of new user by confirmation code",

     *          @SWG\Parameter(
     *          name="code",
     *          description="verification code of user",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *          @SWG\Parameter(
     *          name="phone",
     *          description="phone of user",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),

     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:otps", "read:otps"}
     *         }
     *     },
     * )
     *
     */

    public function verifyOtp(VerifyOtpRequest $request)
    {

        $user = User::Where('mobile_number' , $request->phone)->first();

        if(Otp::where('user_id' , $user->id)->where('code' , $request->code)->where('is_valid' , 0)->first() &&
            User::where('id' , $user->id)->where('mobile_number' , $request->phone)->first() ) {
            $otp = Otp::where('user_id', $user->id)->where('code', $request->code)->where('is_valid', 0)
                ->orderBy('id', 'desc')->first();
            $ot_expired = Carbon::parse($otp->created_at)->addMinutes(2);
           // dd($ot_expired);
            if ($ot_expired >= Carbon::now()) {
                $user->email_verified_at =  Carbon::now() ;
                $user->save();
                $users=User::where('is_admin' , 1)->pluck('id');
                if($user->save())
                    adminNotification(14 , $users , 7 , 2 , $user , apiResponse(200, trans('messages.success')) ,
                        1);
                else
                    adminNotification(14 , $users , 7 , 2 , $user , apiResponse(500, 'error') ,
                        2);

                return apiResponse(200, trans('messages.code_activated'), ['users' =>$user]);
            } else {

                return apiResponse(400, trans('messages.code_expired'));
            }
        }
        else {

            return apiResponse(401, trans('messages.invalid_credentials'));
        }
    }
}
