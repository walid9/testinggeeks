<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Cart\AddToCartRequest;
use App\Http\Requests\Api\Cart\UpdateCartRequest;
use App\Models\Product;
use App\Http\Resources\CartProductsResource;
use App\Http\Resources\ProductsResource;
use \App\Models\User;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class CartController extends Controller
{
    use ApiResponser;


    /**
     * @SWG\Post(
     *      path="/cart/store",
     *      operationId="add products to cart",
     *      tags={"Cart"},
     *      summary="Add products to cart",
     *      description="store product to my cart",
     *      @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="product_id",
     *          description="product id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *    @SWG\Parameter(
     *          name="quantity",
     *          description="product quantity",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:offer", "read:offer"}
     *         }
     *     },
     * )
     *
     */
    public function store(AddToCartRequest $request)
    {
        try{

            $user = \JWTAuth::parseToken()->authenticate();

            if (!$user)
                return apiResponse(401, 'تسجل الدخول مطلوب من اجل هذه العمليه');

            $product = Product::findOrFail($request['product_id']);

            $price = $product->productPrices()->where('id',$request['price_id'])->first();

            if(!$price)
                return apiResponse(400, 'عفوا هذا السعر غير تابع لهدا المنتج');

            if ($request['quantity'] > $price->quantity)
                return apiResponse(400, 'عذرا هذه الكميه غير متوفره لهذا (السعر/الحجم)');

            if ($product->type == 'company' && $product->company_limit_qy > $request['quantity'])
                return apiResponse(400,' عذرا اقل كميه من اجل شركه '.$product->company_limit_qy);

            $checkProductInCart = $user->userCart()->where('product_id',$product->id)
                ->count();

            if ($checkProductInCart){

                $cartItem = $user->userCart()->where('product_id',$product->id)
                    ->first()->pivot;

                $cartItemQty = $request['quantity'] + $cartItem->quantity;

                $qtyInStock = $price->quantity;

                if($qtyInStock < $cartItemQty)
                    return apiResponse(400, ' عفوا لقد تجاوزت الحد الاقصى المسموح به فى الكميه');

                $cartItem->quantity = $cartItemQty;

                $cartItem->save();

                return apiResponse(200, 'تم اضافة المنتج بنجاح');
            }

            $data = [
                'user_id' => $user->id,
                'product_id' => $product->id,
                'quantity' => $request['quantity'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ];

            DB::table('user_cart')->insert($data);

            return apiResponse(200, 'تم اضافة المنتج بنجاح');

        }catch(\Exception $e){

            return apiResponse(400,'sorry something went wrong');
        }
    }


    /**
     * @SWG\Post(
     *      path="/cart/update/{cart_id}",
     *      operationId="update products in cart",
     *      tags={"Cart"},
     *      summary="update products in cart",
     *      description="update products in cart",
     *      @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="cart_id",
     *          description="item id in cart (cart id)",
     *          required=true,
     *          type="integer",
     *          in="path"
     *      ),
     *    @SWG\Parameter(
     *          name="quantity",
     *          description="product quantity",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:offer", "read:offer"}
     *         }
     *     },
     * )
     *
     */
    public function update(UpdateCartRequest $request, $cart_id)
    {
        try{

            $user = \JWTAuth::parseToken()->authenticate();

            $cartItem = DB::table('user_cart')
                ->where('id',$cart_id)
                ->where('user_id',$user->id)
                ->first();

            if (!$cartItem)
                return apiResponse(400, 'عذرا هذا العنصر غير متوفر فى سله المشتريات الخاصه بك');

            $product = Product::findOrFail($cartItem->product_id);

            if ($request['quantity'] > $product->quantity)
                return apiResponse(400, 'عذرا هذه الكميه غير متوفره');

            $data = [];
            $data['quantity'] = $request['quantity'];

            $cartItem = DB::table('user_cart')
                ->where('id',$cart_id)
                ->where('user_id',$user->id)
                ->update($data);

            return apiResponse(200, 'تم تعديل المنتج بنجاح');


        }catch (\Exception $e){

            return apiResponse(400,'sorry something went wrong');
        }

    }

    /**
     * @SWG\Post(
     *      path="/cart/delete/{cart_id}",
     *      operationId="delete product from cart",
     *      tags={"Cart"},
     *      summary="delete product from cart",
     *      description="delete product from cart",
     *      @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="cart_id",
     *          description="cart_id id of item in cart",
     *          required=true,
     *          type="integer",
     *          in="path"
     *      ),
     *
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:offer", "read:offer"}
     *         }
     *     },
     * )
     *
     */
    public function destroy(Request $request, $cart_id)
    {
        try{

            $user = \JWTAuth::parseToken()->authenticate();

            $cartItem = DB::table('user_cart')
                ->where('id',$cart_id)
                ->where('user_id',$user->id);

            if (!$cartItem->first())
                return apiResponse(400, 'عذرا هذا العنصر غير متوفر فى سله المشتريات الخاصه بك');

            $cartItem->delete();

            return apiResponse(200, 'تم حذف المنتج بنجاح');

        }catch (\Exception $e){

            return apiResponse(400,'sorry something went wrong');
        }
    }

    /**
     * @SWG\Post(
     *      path="/cart/destroy",
     *      operationId="delete cart",
     *      tags={"Cart"},
     *      summary="delete cart",
     *      description="delete cart",
     *      @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:offer", "read:offer"}
     *         }
     *     },
     * )
     *
     */
    public function delete(Request $request)
    {
        try{

            $user = \JWTAuth::parseToken()->authenticate();

            $cartItem = DB::table('user_cart')
                ->where('user_id',$user->id);

            if (!$cartItem->first())
                return apiResponse(400, 'عذرا لا توجد سله المشتريات خاصه بك');

                 DB::table('user_cart')->where('user_id',$user->id)->delete();


            return apiResponse(200, 'تم حذف سلة المشتريات الخاصة بك  بنجاح');

        }catch (\Exception $e){

            return apiResponse(400,'sorry something went wrong');
        }
    }

    /**
     * @SWG\Get(
     *      path="/cart",
     *      operationId="show user cart",
     *      tags={"Cart"},
     *      summary="show products in cart",
     *      description="show products from cart",
     *      @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *
     *   @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=false,
     *          type="string",
     *          in="header"
     *      ),
     *
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:offer", "read:offer"}
     *         }
     *     },
     * )
     *
     */
    public function cart(){

        try{

            $user = \JWTAuth::parseToken()->authenticate();

            $userCartProduct = $user->userCart;

            return apiResponse(200,'Success', CartProductsResource::collection($userCartProduct));

        }catch (Exception $e){

            return apiResponse(400,'sorry something went wrong');
        }
    }

    /**
     * @SWG\Get(
     *      path="/cart/details",
     *      operationId="show user cart details",
     *      tags={"Cart"},
     *      summary="show cart detials (item count and total)",
     *      description="show cart detials (item count and cart total)",
     *      @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:offer", "read:offer"}
     *         }
     *     },
     * )
     *
     */
    public function details(){

        try{

            $user = \JWTAuth::parseToken()->authenticate();

            $userCartProduct = $user->userCart;

            $total = 0;

            foreach($userCartProduct as $product){

                $result = ($product->selling_price * $product->pivot->quantity);

                $total +=  round($result , 2);
            }


            $count = $userCartProduct->count();

            return apiResponse(200,'Success', ['cart_total'=> $total, 'items_count'=> $count]);

        }catch (Exception $e){

            return apiResponse(400,'sorry something went wrong');
        }

    }


}
