<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Stores\CreateStoresRequest;
use App\Http\Requests\Api\Stores\UpdateStoreRequest;
use App\Http\Resources\ProductDetailsResource;
use App\Http\Resources\ProductsResource;
use App\Http\Resources\StoresResource;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Exceptions\JWTException;

class StoresController extends Controller
{
    /**
     * @SWG\Get(
     *      path="/stores/",
     *      operationId="get stores",
     *      tags={"Stores"},
     *      summary="Get stores information",
     *      description="Returns stores data in home",
     *      @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:Products", "read:Products"}
     *         }
     *     },
     * )
     *
     */
    public function index()
    {
        $products = Store::all();
        return apiResponse('200','تم بنجاح',StoresResource::collection($products),true,10);
    }

    /**
     * @SWG\Post(
     *      path="/stores/store",
     *      operationId="create stores",
     *      tags={"Stores"},
     *      summary="create stores information",
     *      description="create stores data",
     *      @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="name",
     *          description="name",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="description",
     *          description="description",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:offer", "read:offer"}
     *         }
     *     },
     * )
     *
     */
    public function store(CreateStoresRequest $request)
    {
        try{

            $user = \JWTAuth::parseToken()->authenticate();

            if (!$user)
                return apiResponse(401, 'تسجل الدخول مطلوب من اجل هذه العمليه');

            $data = [
                'user_id' => $user->id,
                'name' => $request->name,
                'description' => $request->description,
            ];

            DB::table('stores')->insert($data);

            return apiResponse(200, 'تم اضافة المنتج بنجاح');

        }catch(\Exception $e){

            return apiResponse(400,'sorry something went wrong');
        }
    }

    /**
     * @SWG\Post(
     *      path="/stores/update",
     *      operationId="update stores",
     *      tags={"Stores"},
     *      summary="update stores information",
     *      description="update stores data",
     *      @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="name",
     *          description="name",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="description",
     *          description="description",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="store_id",
     *          description="store_id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:offer", "read:offer"}
     *         }
     *     },
     * )
     *
     */
    public function update(UpdateStoreRequest $request)
    {
        try{

            $user = \JWTAuth::parseToken()->authenticate();

            if (!$user)
                return apiResponse(401, 'تسجل الدخول مطلوب من اجل هذه العمليه');

            $store = Store::find($request['store_id']);

            $data = [
                'name' => $request->name,
                'description' => $request->description,
            ];

            $store->update($data);

            return apiResponse(200, 'تم تحديث المعرض بنجاح');

        }catch(\Exception $e){

            return apiResponse(400,'sorry something went wrong');
        }
    }

    /**
     * @SWG\Post(
     *      path="/stores/delete",
     *      operationId="delete stores",
     *      tags={"Stores"},
     *      summary="delete stores information",
     *      description="delete stores data",
     *      @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="store_id",
     *          description="store_id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:offer", "read:offer"}
     *         }
     *     },
     * )
     *
     */
    public function delete(Request $request)
    {
        try{

            $user = \JWTAuth::parseToken()->authenticate();

            if (!$user)
                return apiResponse(401, 'تسجل الدخول مطلوب من اجل هذه العمليه');

            $store = Store::find($request['store_id']);
            $store->delete();

            return apiResponse(200, 'تم حذف المعرض بنجاح');

        }catch(\Exception $e){

            return apiResponse(400,'sorry something went wrong');
        }
    }
}
