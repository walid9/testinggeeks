<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\CreateProductReviewRequest;
use App\Http\Resources\CategoriesResources;
use App\Http\Resources\OfferHomeResource;
use App\Http\Resources\OfferResources;
use App\Http\Resources\ProductDetailsResource;
use App\Http\Resources\ProductsResource;
use App\Http\Resources\ReviewsResource;
use App\Models\Category;
use App\Models\Offer;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\Product;
use App\Models\ProductReview;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Exceptions\JWTException;

class ProductsController extends Controller
{
    /**
     * @SWG\Get(
     *      path="/products/",
     *      operationId="get products",
     *      tags={"Products"},
     *      summary="Get products information",
     *      description="Returns products data in home",
     *      @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:Products", "read:Products"}
     *         }
     *     },
     * )
     *
     */
    public function index()
    {
        $products = Product::all();
        return apiResponse('200','تم بنجاح',ProductsResource::collection($products),true,10);
    }

    /**
     * @SWG\Post(
     *      path="/products/store",
     *      operationId="store products",
     *      tags={"Products"},
     *      summary="store products information",
     *      description="store products data",
     *      @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="name",
     *          description="name",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="description",
     *          description="description",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="selling_price",
     *          description="selling_price",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="purchasing_price",
     *          description="purchasing_price",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="store_id",
     *          description="store_id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *    @SWG\Parameter(
     *          name="quantity",
     *          description="product quantity",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:offer", "read:offer"}
     *         }
     *     },
     * )
     *
     */
    public function store(ProductsRequest $request)
    {
        try{

            $user = \JWTAuth::parseToken()->authenticate();

            if (!$user)
                return apiResponse(401, 'تسجل الدخول مطلوب من اجل هذه العمليه');

            $data = [
                'store_id' => $request->store_id,
                'name' => $request->name,
                'description' => $request->description,
                'selling_price' => $request->selling_price,
                'purchasing_price' => $request->purchasing_price,
                'quantity' => $request['quantity'],
            ];

            DB::table('products')->insert($data);

            return apiResponse(200, 'تم اضافة المنتج بنجاح');

        }catch(\Exception $e){

            return apiResponse(400,'sorry something went wrong');
        }
    }

    /**
     * @SWG\Post(
     *      path="/products/update",
     *      operationId="update products",
     *      tags={"Products"},
     *      summary="update products information",
     *      description="update products data",
     *      @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="name",
     *          description="name",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="description",
     *          description="description",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="selling_price",
     *          description="selling_price",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="purchasing_price",
     *          description="purchasing_price",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="product_id",
     *          description="product_id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *    @SWG\Parameter(
     *          name="quantity",
     *          description="product quantity",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:offer", "read:offer"}
     *         }
     *     },
     * )
     *
     */
    public function update(UpdateProductRequest $request)
    {
        try{

            $user = \JWTAuth::parseToken()->authenticate();

            if (!$user)
                return apiResponse(401, 'تسجل الدخول مطلوب من اجل هذه العمليه');

            $product = Product::find($request['product_id']);

            $data = [
                'name' => $request->name,
                'description' => $request->description,
                'selling_price' => $request->selling_price,
                'purchasing_price' => $request->purchasing_price,
                'quantity' => $request['quantity'],
            ];

            $product->update($data);

            return apiResponse(200, 'تم تحديث المنتج بنجاح');

        }catch(\Exception $e){

            return apiResponse(400,'sorry something went wrong');
        }
    }

    /**
     * @SWG\Post(
     *      path="/products/delete",
     *      operationId="delete products",
     *      tags={"Products"},
     *      summary="delete products information",
     *      description="delete products data",
     *      @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="product_id",
     *          description="product_id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:offer", "read:offer"}
     *         }
     *     },
     * )
     *
     */
    public function delete(Request $request)
    {
        try{

            $user = \JWTAuth::parseToken()->authenticate();

            if (!$user)
                return apiResponse(401, 'تسجل الدخول مطلوب من اجل هذه العمليه');

            $product = Product::find($request['product_id']);
            $product->delete();

            return apiResponse(200, 'تم حذف المنتج بنجاح');

        }catch(\Exception $e){

            return apiResponse(400,'sorry something went wrong');
        }
    }
}
