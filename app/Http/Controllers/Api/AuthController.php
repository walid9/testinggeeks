<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Auth\CreateUserRequest;
use App\Http\Requests\Api\Auth\ForgetPasswordRequest;
use App\Http\Requests\Api\Auth\LoginUserRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Mockery\Exception;
use phpDocumentor\Reflection\Types\Null_;
use Tymon\JWTAuth\Exceptions\JWTException;
//use Illuminate\Support\Facades\Hash;
class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('api.authenticatable', ['only' => ['logout']]);
    }

    use ApiResponser;

    /**
     * @SWG\Post(
     *      path="/auth/update-firebase-token",
     *      operationId="update firebase_token for user",
     *      tags={"Authenticate"},
     *      summary="update firebase_token",
     *      description="update firebase_token",
     *          @SWG\Parameter(
     *          name="authorization",
     *          description="authorization token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *          @SWG\Parameter(
     *          name="firebase_token",
     *          description="firebase_token",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function updateFirebase(Request $request)
    {

        if (request()->header('authorization')) {

            try {

                $user = \JWTAuth::parseToken()->authenticate();

            } catch (JWTException $e) {

                return apiResponse(500, 'كود التسجيل خطا ، من فضلك ادخل الكود الصحيح');
            }
        } else
            return apiResponse(401, 'تسجيل الدخول مطلوب من اجل هذه العمليه');

        $token = User::where('id', $user->id)->first();

        $token->update(['firebase_token' => $request->firebase_token]);
        return apiResponse(200, trans('Success'));

    }

    /**
     * @SWG\Post(
     *      path="/auth/register",
     *      operationId="user regestration",
     *      tags={"Authenticate"},
     *      summary="user regestration",
     *      description="Returns user Data",
     *
     *    @SWG\Parameter(
     *          name="device_id",
     *          description="device_id",
     *          required=false,
     *          type="string",
     *          in="formData"
     *      ),
     *     @SWG\Parameter(
     *          name="name",
     *          description="User name",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *
     *     @SWG\Parameter(
     *          name="email",
     *          description="User email",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *
     *     @SWG\Parameter(
     *          name="mobile_number",
     *          description="User mobile_number",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *
     *     @SWG\Parameter(
     *          name="type",
     *          description="type 0 is guest 1 is merchant",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *
     *     @SWG\Parameter(
     *          name="password",
     *          description="User password",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */

    public function register(CreateUserRequest $request)
    {
        try{

            $data = $request->only('device_id','name','email','mobile_number',
                'type');


            $data['password'] = \Illuminate\Support\Facades\Hash::make($request['password']);

            $user = User::where('device_id', $request['device_id'])->where('email', null)->first();

            if (User::where('email', $request['email'])->where('email_verified_at' , '!=' , null)->first())
                return apiResponse(401, trans('البريد الالكترونى مستخدم من قبل'));

            if (User::where('mobile_number', $request['mobile_number'])->where('email_verified_at' , '!=' , null)->first())
                return apiResponse(401, trans('رقم الهاتف مستخدم من قبل'));

            if (User::where('email', $request['email'])->where('email_verified_at' , null)->first())
            {
                $user = User::where('email', $request['email'])->where('email_verified_at' , null)->first();
                $user->update($data);
                return apiResponse(200, trans('Success'), new UserResource($user));
            }

            if (User::where('mobile_number', $request['mobile_number'])->where('email_verified_at' , null)->first())
            {
                $user = User::where('mobile_number', $request['mobile_number'])->where('email_verified_at' , null)->first();
                $user->update($data);
                return apiResponse(200, trans('Success'), new UserResource($user));
            }


            if(!$user){

                    $user = User::create($data);

            }else{

                $user->update($data);
            }

        }catch(\Exception $e){

//            return $e->getMessage();
            return apiResponse(400, trans('error'), 'sorry something went wrong');
        }

        return apiResponse(200, trans('Success'), new UserResource($user));

    }

    /**
     * @SWG\Post(
     *      path="/auth/login",
     *      operationId="user Login",
     *      tags={"Authenticate"},
     *      summary="user Login",
     *      description="Returns user Data",
     *      @SWG\Parameter(
     *          name="data",
     *          description=" Mobile Or Email",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *
     *      @SWG\Parameter(
     *          name="password",
     *          description="User password",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function login(LoginUserRequest $request)
    {
        $user = User::whereMobileNumber($request->data)->orWhere('email', $request->data)->first();

        if ( $user && \Illuminate\Support\Facades\Hash::check($request->password, $user->password)) {

            if (!$user->email_verified_at) {
                return apiResponse(403, trans('messages.activation_required'));
            } elseif ($user->active == 0) {
                return apiResponse(405, trans('messages.user_blocked'));
            }
            $token = auth('api')->login($user);
            return apiResponse(200, trans('Success'), ['token' => $token, 'users' => $user]);
        } else {

            return apiResponse(405, trans('messages.invalid_credentials'));
        }
    }


    /**
     * @SWG\Patch(
     *      path="/auth/update-password",
     *      operationId="Update Password",
     *      tags={"Authenticate"},
     *      summary="Update Password",
     *      description="Returns new password",
     *       @SWG\Parameter(
     *          name="authorization",
     *          description="authorization token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *
     *      @SWG\Parameter(
     *          name="oldPassword",
     *          description="Old password",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *        @SWG\Parameter(
     *          name="newPassword",
     *          description="New password",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function updatePassword(Request $request)
    {
        $user = \JWTAuth::parseToken()->authenticate();
        if ($user) {
            if($user->email_verified_at) {
                if (\Illuminate\Support\Facades\Hash::check($request->oldPassword, $user->password)) {
                    $user->update(['password' => \Illuminate\Support\Facades\Hash::make($request['newPassword'])]);
                    return apiResponse(200, trans('تم تعديل كلمة السر بنجاح'), ['user' => $user]);
                } else {
                    return apiResponse(403, trans('كلمة السر القديمه غير صحيحه'));
                }
            }
            else
            {
                return apiResponse(403, trans('لابد من تفعيل الاكونت اولا'));
            }
        } else {
            return apiResponse(403, trans('لا يوجد بيانات لهذا المستخدم برجاء التسجيل'));
        }

    }


    /**
     * @SWG\Post(
     *      path="/auth/forget-password",
     *      operationId="user forget-password",
     *      tags={"Authenticate"},
     *      summary="user forget-password",
     *      description="Returns Code",
     *      @SWG\Parameter(
     *          name="email",
     *          description="User email",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function forgetPassword(ForgetPasswordRequest $request)
    {
        $user = User::whereMobileNumber($request->email)->orWhere('email', $request->email)->first();
        //$admin_email = DB::table('settings')->where('name', 'admin_email')->value('value');
           $admin_email = "bwardy.bander@gmail.com";
        if ($user && $admin_email) {
            $code = Crypt::encrypt(generateRandomCode(4, $user->id));
            $email = $user->email;

            $user->code = Crypt::decrypt($code);
            $user->save();

            try {

                //$this->sendMessage($user->mobile_number,'The verification code is '.Crypt::decrypt($code) );

                @Mail::send([], [], function ($message) use ($email, $admin_email, $code) {
                    $message->to($email)
                        ->subject('Mhsol')
                        ->setBody("Code Is : " . Crypt::decrypt($code));
                    $message->from($admin_email);
                });
        }
        catch (\Exception $e) {

                return apiResponse(200, trans('Success'), Crypt::decrypt($code));
        }
            return apiResponse(200, trans('Success'), Crypt::decrypt($code));
        } else {
            return apiResponse(405, trans('Error  Check Your Email Or Mobile number'));
        }


    }

    /**
     * @SWG\Post(
     *      path="/auth/change-password",
     *      operationId="user change-password",
     *      tags={"Authenticate"},
     *      summary="user change-password",
     *      description="Returns New Password",
     *      @SWG\Parameter(
     *          name="email",
     *          description="User email",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *
     *       @SWG\Parameter(
     *          name="password",
     *          description="New password",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *
     *       @SWG\Parameter(
     *          name="code",
     *          description="User Code",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function changePassword(Request $request)
    {
        $user = User::whereMobileNumber($request->email)->orWhere('email', $request->email)->first();

        $token = auth('api')->login($user);

        if ($user) {
            if ($user->code == $request->code) {

                $new_password = Crypt::encrypt($request->password);
               // $admin_email = DB::table('settings')->where('name', 'admin_email')->value('value');
                $admin_email = "bwardy.bander@gmail.com";
                if ($admin_email) {
                    $email = $user->email;

                    $user->password = \Illuminate\Support\Facades\Hash::make($request['password']);
                    $user->save();

                    try {
                        @Mail::send([], [], function ($message) use ($email, $admin_email, $new_password) {

                            $message->to($email)
                                ->subject('Mhsol')
                                ->setBody("New Password Is : " . Crypt::decrypt($new_password));
                            $message->from($admin_email);
                        });
                    }
                    catch (\Exception $e) {

                        return apiResponse(200, trans('Success'), ['token' => $token]);
                    }

                    return apiResponse(200, trans('Success'), ['token' => $token]);

                } else {
                    return apiResponse(405, trans('Error Email Or Mobile number'));
                }

            } else {
                return apiResponse(401, trans('Invalid_code'));
            }
        } else {
            return apiResponse(405, trans('Error Email Or Mobile number'));
        }

    }

    /**
     * @SWG\Post(
     *      path="/auth/logout",
     *      operationId="user logout",
     *      tags={"Authenticate"},
     *      summary="user logout",
     *      description="Returns user logout",
     *          @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function logout()
    {
        if(\JWTAuth::parseToken()->authenticate()) {
            auth('api')->logout();
            return apiResponse(200, trans('messages.success'));
        }
    }




    /**
     * @SWG\Post(
     *      path="/auth/skip-login",
     *      operationId="skip login , create new user",
     *      tags={"Authenticate"},
     *      summary="skip login , create new user",
     *      description="skip login , create new user",

     *          @SWG\Parameter(
     *          name="device_id",
     *          description="device_id",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function skipLogin(Request $request)
    {
        $this->validate($request,[
           'device_id'=>'required|string',
        ]);

        try {
            DB::beginTransaction();

            $user = User::where('device_id',$request['device_id'])->where('email',null)->first();

            if($user)
                $token = \JWTAuth::fromUser($user);

            else{

                $user = User::create([
                    'device_id'=> $request['device_id'],
                ]);

                $token = \JWTAuth::fromUser($user);
            }

            DB::commit();
        } catch (JWTException $e) {

            DB::rollBack();
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        return apiResponse(200, 'Success', $token);
    }


}
