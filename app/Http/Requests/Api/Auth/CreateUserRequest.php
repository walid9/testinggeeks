<?php

namespace App\Http\Requests\Api\Auth;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'name' => 'required|string|max:150',
            'email' => 'required|email',
            'password' => 'required|string|min:6',
            'mobile_number' => 'required|string',
            'type' => 'required|integer|in:0,1',

        ];
    }
    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response(apiResponse(405, $validator->errors()->first())));
    }
}
