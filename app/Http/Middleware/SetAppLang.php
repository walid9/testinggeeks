<?php

namespace App\Http\Middleware;

use Closure;

class SetAppLang
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $lang = $request->header('lang');
        if ($lang) {
            app()->setLocale($lang);
        }
        return $next($request);
    }
}
