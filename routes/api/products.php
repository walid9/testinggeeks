<?php

Route::get('', 'ProductsController@index');
Route::get('store', 'ProductsController@store');
Route::get('update', 'ProductsController@update');
Route::get('delete', 'ProductsController@getRelated');
