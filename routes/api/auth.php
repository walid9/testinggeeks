
<?php
Route::post('register', 'AuthController@register');
Route::post('login', 'AuthController@login');
Route::post('logout', 'AuthController@logout');
Route::post('forget-password', 'AuthController@forgetPassword');
Route::post('change-password', 'AuthController@changePassword');
Route::post('update-firebase-token', 'AuthController@updateFirebase');
Route::Patch('update-password', 'AuthController@updatePassword');
Route::post('skip-login', 'AuthController@skipLogin');
