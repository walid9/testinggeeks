<?php

Route::get('', 'ProfileController@index');
Route::post('update', 'ProfileController@update');
Route::post('uploadImage', 'ProfileController@uploadImage');
