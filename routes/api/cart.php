<?php
Route::get('', 'CartController@cart');
Route::post('/store', 'CartController@store');
Route::post('/update/{cart_id}', 'CartController@update');
Route::post('/delete/{cart_id}', 'CartController@destroy');
Route::post('/destroy', 'CartController@delete');
Route::get('/details', 'CartController@details');
