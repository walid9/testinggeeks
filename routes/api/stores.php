<?php

Route::get('', 'StoresController@index');
Route::get('store', 'StoresController@store');
Route::get('update', 'StoresController@update');
Route::get('delete', 'StoresController@getRelated');
