<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesPermissionsSeeder::class);

        $this->call(settingSeeder::class);

        $this->call(ProductExtraClassificationsTableSeeder::class);
        $this->call(ProductSizesTableSeeder::class);

        $this->call(PaymentTypesSeeder::class);
        $this->call(InvoiceStatusSeeder::class);
        $this->call(OrderStatusSeeder::class);
        $this->call(CuttingTypesSeeder::class);
        $this->call(CuttingSeeder::class);
        $this->call(ShippingStatusSeeder::class);
        $this->call(OfferTypesSeeder::class);
        $this->call(SupportTypesSeeder::class);
        $this->call(CountryCodeSeeder::class);
        $this->call(ChannelSeeder::class);
        $this->call(LanguageSeeder::class);
        $this->call(NotificationKeySeeder::class);
        $this->call(NotificationTypeSeeder::class);
        $this->call(SendingStatusSeeder::class);
        $this->call(NotificationLocalesSeeder::class);
        $this->call(OtherSettingSeeder::class);
        $this->call(SizeSeeder::class);
        $this->call(WhatsSettingSeeder::class);
        $this->call(InvoiceSeeder::class);
    }
}
