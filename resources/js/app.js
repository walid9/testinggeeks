/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import Multiselect from 'vue-multiselect';

window.Vue = require('vue');
import VueLazyload from 'vue-lazyload';

Vue.use(VueLazyload);

Vue.component('multiselect', Multiselect);
import {
    ValidationProvider,
    extend
} from 'vee-validate';
import AttributesComponent from './components/AttributesComponent';
import ProductsComponent from './components/ProductsComponent';
Vue.component('ValidationProvider', ValidationProvider);

import {
    required
} from 'vee-validate/dist/rules';

extend('required', {
    ...required,
    message: 'برجاء إدخال قيمه هذا الحقل'
});
const app = new Vue({
    el: '#app',
    components: {
        'attributes-component': AttributesComponent,
        'products-component': ProductsComponent
    }
});
